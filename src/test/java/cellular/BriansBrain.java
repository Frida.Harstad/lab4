package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;


public class BriansBrain implements CellAutomaton {
	
	IGrid currentGeneration;

	public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		for (int i = 0; i < numberOfRows(); i++) {
			for (int j = 0; j < numberOfColumns(); j++) {
				nextGeneration.set(i, j, getNextCell(i,j));
			}
		}
		currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		CellState currentCell = getCellState(row, col);
		int aliveCell = countNeighbors(row, col, CellState.ALIVE);
		
		if (currentCell == CellState.ALIVE) {
					return CellState.DYING;			
		} else if (currentCell == CellState.DYING) {
			return CellState.DEAD;
		} else if (aliveCell == 2 && currentCell == CellState.DEAD){
			return CellState.ALIVE;
		} else {
			return CellState.DEAD;
		}
	}

	
	private int countNeighbors(int row, int col, CellState state) {
		int numNeighbors = 0;
		
		for (int i = row-1; i <= row+1; i++) {
			for (int j = col-1; j <= col+1; j++) {
				if (i == row && j == col) {
					continue;
				} else {
					if ((i >= 0 && i <= currentGeneration.numRows())
							&& (j >= 0 && j <= currentGeneration.numColumns())) {
						if (getCellState(row, col).equals(state)) {
							numNeighbors++;
						}
					}
				}
			}
		}
		return numNeighbors;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}

