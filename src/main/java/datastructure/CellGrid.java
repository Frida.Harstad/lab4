package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
	
	int cols;
	int rows;
	CellState initialState;
	CellState cellGrid[][];
	
	
	
    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
		this.cols = columns;
		this.initialState = initialState;
		this.cellGrid = new CellState[rows][cols];
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
    	cellGrid[row][column] = element;
        
    }

    @Override
    public CellState get(int row, int column) {
        return cellGrid[row][column];
    }

    @Override
    public IGrid copy() {
        IGrid copyGrid = new CellGrid(rows, cols, initialState);
        for (int row = 0; row < numRows(); row++) {
        	for (int col = 0; col < numColumns(); col++) {
        		copyGrid.set(row, col, cellGrid[row][col]);
        	}
        }
        return copyGrid;
    }
    
}
